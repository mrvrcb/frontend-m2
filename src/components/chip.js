import '../style/components/chip.css';

import PropTypes from 'prop-types';
import React from 'react';

const Chip = ({ name, onClick }) => (
  <div className="chip" onClick={onClick}>
    {name}
  </div>
);

Chip.propTypes = {
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

Chip.defaultProps = {
  onClick: () => {}
};

export default Chip;
