import '../style/pages/product-details.css';

import React, { Component } from 'react';
import { indexBy, map, pipe, prop, sum } from 'ramda';
import { inject, observer, propTypes } from 'mobx-react';

import Button from '../components/button';
import DefaultThumbnail from '../images/default-thumbnail.png';
import ExtraField from '../components/extra-field';
import FormattedCurrency from '../components/formatted-currency';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { contains } from 'ramda';
import { values } from 'ramda';
import { withRouter } from 'react-router-dom';

class ProductDetails extends Component {
  state = {
    selectedExtras: []
  };

  componentWillMount() {
    const { match } = this.props;
    const { params } = match;
    const { id } = params;

    this.props.productStore.getDetails(id);
  }

  get isAdminProfile() {
    const { selected } = this.props.userStore;

    if (!selected) {
      return false;
    }

    return contains('ADMIN', selected.roles.map(role => role.name));
  }

  get orderTotal() {
    const { productStore } = this.props;
    const { selected } = productStore;
    const { extras, price } = selected;
    const { selectedExtras } = this.state;

    const indexedExtras = indexBy(prop('id'), extras);
    const calculateExtraTotal = pipe(
      map(selectedExtra => indexedExtras[selectedExtra]),
      map(extra => extra.price.value),
      sum
    );

    const extrasTotal = calculateExtraTotal(selectedExtras);

    return {
      product: {
        ...price
      },
      extras: {
        currency: 'BRL',
        value: extrasTotal
      },
      total: {
        currency: 'BRL',
        value: extrasTotal + price.value
      }
    };
  }

  handleEditClick = () => {
    const {
      history,
      productStore: {
        selected: { id }
      }
    } = this.props;

    history.push(`/edit-product/${id}`);
  };

  handleOrderClick = () => {
    const { productStore, userStore } = this.props;
    const { selected } = productStore;
    const { selectedExtras = [] } = this.state;

    console.log({
      userId: userStore.selected,
      productId: selected.id,
      selectedExtras
    });
  };

  handleExtraSelection = extraId => event => {
    const { selectedExtras } = this.state;

    if (event.target.checked) {
      this.setState({
        selectedExtras: [...selectedExtras, extraId]
      });
    } else {
      this.setState({
        selectedExtras: this.state.selectedExtras.filter(
          selectedId => selectedId !== extraId
        )
      });
    }
  };

  renderActions = () => (
    <div className="product-details__section order-actions">
      <div className="order-actions__item">
        <Button
          type="submit"
          message="Encomendar"
          onClick={this.handleOrderClick}
        />
      </div>
      <div className="order-actions__item">
        {this.isAdminProfile && (
          <Button
            type="button"
            message="Editar"
            onClick={this.handleEditClick}
          />
        )}
      </div>
    </div>
  );

  renderOrderTotal = () => {
    const { extras, product, total } = this.orderTotal;

    return (
      <div className="product-details__section total-container">
        <div className="total-container__title">Total do pedido</div>

        <div className="total-container__content order-total">
          <div className="order-total__info">
            <div className="order-total__product">
              <FormattedCurrency {...product} />
            </div>

            <div className="order-total__extras">
              <FormattedCurrency {...extras} />
            </div>
          </div>

          <div className="order-total__total">
            <FormattedCurrency {...total} />
          </div>
        </div>
      </div>
    );
  };

  renderProductExtras = () => {
    const { productStore } = this.props;
    const { selected } = productStore;
    const { extras } = selected;

    const productExtras = values(extras.toJSON());

    return (
      <div className="product-details__section extras-container">
        <div className="extras-container__title">Adicionais</div>

        <div className="extras-container__items">
          {productExtras.map(({ id, name, price }) => (
            <ExtraField
              key={id}
              price={price}
              displayName={name}
              name={`extras.${id}`}
              className="extras-container__item"
              onChange={this.handleExtraSelection(id)}
            />
          ))}
        </div>
      </div>
    );
  };

  renderProductDetails = () => {
    const { productStore } = this.props;
    const { selected } = productStore;
    const { category, description, name, price, thumbnail } = selected;

    return (
      <div className="product-details__section details-container">
        <div className="details-container__thumbnail">
          <img src={thumbnail || DefaultThumbnail} alt="Imagem do produto" />
        </div>

        <div className="details-container__main details">
          {category && <div className="details__category">{category.name}</div>}

          <div className="details__name">{name}</div>

          <div className="details__description">{description}</div>

          <div className="details__price">
            <FormattedCurrency {...price} />
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { productStore } = this.props;
    const { isLoading } = productStore;

    return (
      <div className="product-details">
        {isLoading && <div>Loading...</div>}
        {!isLoading && this.renderProductDetails()}
        {!isLoading && this.renderProductExtras()}
        {!isLoading && this.renderOrderTotal()}
        {!isLoading && this.renderActions()}
      </div>
    );
  }
}

ProductDetails.propTypes = {
  productStore: PropTypes.shape({
    items: propTypes.observableMap.isRequired,
    state: PropTypes.string.isRequired,
    selected: PropTypes.object
  }),
  history: PropTypes.object
};

const enhance = compose(
  inject('productStore', 'userStore'),
  observer
);

export default withRouter(enhance(ProductDetails));
