import '../style/components/logo.css';

import React from 'react';
import whiteLogo from '../images/white-logo.png';
import blackLogo from '../images/logo.png';

const Logo = ({ black }) => (
  <img
    className="logo-img"
    src={black ? blackLogo : whiteLogo}
    alt="Candy Shop Logo"
  />
);

export default Logo;
