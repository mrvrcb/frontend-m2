import { Money } from './money';
import { types } from 'mobx-state-tree';

export const Extra = types.model({
  id: types.identifier(types.string),
  name: types.string,
  price: types.reference(Money)
});
