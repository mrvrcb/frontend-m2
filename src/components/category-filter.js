import '../style/components/category-filter.css';

import React, { Component } from 'react';
import { inject, observer, propTypes } from 'mobx-react';

import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { values } from 'ramda';

class CategoryFilter extends Component {
  static propTypes = {
    className: PropTypes.string,
    categoryStore: PropTypes.shape({
      items: propTypes.observableMap.isRequired,
      state: PropTypes.string.isRequired
    })
  };

  static defaultProps = {
    className: ''
  };

  state = {
    selected: null
  };

  isSelected = id => {
    return this.state.selected === id;
  };

  componentWillMount() {
    this.props.categoryStore.fetchCategories();
  }

  handleCategoryClick = id => () => {
    if (this.state.selected === id) {
      return;
    }

    this.setState({
      selected: id
    });

    this.props.categoryStore.getCategory(id);
  };

  renderItem = ({ id, name }) => {
    let targetClassNames = ['category-filter__item', 'filter-item'];

    if (this.isSelected(id)) {
      targetClassNames.push('category-filter__item--active');
    }

    return (
      <button
        key={id}
        type="button"
        onClick={this.handleCategoryClick(id)}
        className={targetClassNames.join(' ')}
      >
        {name}
      </button>
    );
  };

  render() {
    const { categoryStore, className } = this.props;
    const { items } = categoryStore;

    const categories = values(items.toJSON());

    return (
      <div className={`category-filter ${className}`}>
        <div className="category-filter__items">
          {categories.map(this.renderItem)}
        </div>
      </div>
    );
  }
}

const enhance = compose(
  inject('categoryStore'),
  observer
);

export default enhance(CategoryFilter);
