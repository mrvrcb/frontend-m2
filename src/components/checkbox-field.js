import '../style/components/checkbox-field.css';

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const CheckboxField = ({ className, onChange, name }) => (
  <input
    name={name}
    type="checkbox"
    onChange={onChange}
    className={classNames('checkbox', { [className]: !!className })}
  />
);

CheckboxField.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

CheckboxField.defaultProps = {
  className: null
};

export default CheckboxField;
