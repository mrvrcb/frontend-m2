import { httpClient } from './http-client';

export const LoginAPI = {
  async login(email, password) {
    try {
      const { status } = await httpClient.post(
        '/login',
        { email, password },
        { withCredentials: true }
      );
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
};
