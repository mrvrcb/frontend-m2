import { httpClient } from './http-client';

export const CategoryAPI = {
  async delete(id) {
    try {
      const { status } = await httpClient.delete(`/categories/${id}`);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async edit(id, values) {
    try {
      const { status } = await httpClient.put(`/categories/${id}`, values);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async create(categoryDetails) {
    try {
      const { status } = await httpClient.post('/categories', categoryDetails);
      return status === 201;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async get(id) {
    try {
      const { data } = await httpClient.get(`/categories/${id}`);
      return data;
    } catch (error) {
      console.error(error);
      return {};
    }
  },

  async list() {
    try {
      const { data } = await httpClient.get('/categories');
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
};
