import { Product } from './product';
import { types } from 'mobx-state-tree';

export const Category = types.model({
  id: types.identifier(types.string),
  name: types.string,
  description: types.optional(types.string, ''),
  products: types.optional(types.array(Product), [])
});
