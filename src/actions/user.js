import { applySnapshot, flow, getSnapshot } from 'mobx-state-tree';
import { indexBy, prop } from 'ramda';
import { reaction, when } from 'mobx';

import { LoginAPI } from '../api/login';
import { UserAPI } from '../api/user';

export const UserActions = self => {
  const afterCreate = () => {
    if (typeof window !== 'undefined' && window.localStorage) {
      when(
        () => self.state === 'loaded',
        () => {
          self.readFromLocalStorage();
          reaction(
            () => getSnapshot(self),
            json => {
              window.localStorage.setItem('user', JSON.stringify(json));
            }
          );
        }
      );
    }
  };

  const readFromLocalStorage = () => {
    const userData = window.localStorage.getItem('user');

    if (userData) {
      applySnapshot(self, JSON.parse(userData));
    }
  };

  const login = flow(function*(username, password) {
    self.state = 'loading';

    const loginSucceed = yield LoginAPI.login(username, password);
    if (!loginSucceed) {
      self.state = 'error';
      self.selected = null;
    } else {
      const userDetails = yield UserAPI.get();
      if (userDetails !== {}) {
        self.state = 'loaded';
        self.selected = userDetails;
      } else {
        self.state = 'error';
        self.selected = null;
      }
    }
  });

  const fetchUsers = flow(function*() {
    self.state = 'loading';

    const items = yield UserAPI.list();
    self.state = 'loaded';
    self.items = indexBy(prop('id'), items);
  });

  const getUser = flow(function*(id) {
    self.state = 'loading';

    const userDetails = yield UserAPI.get(id);

    self.state = 'loaded';
    self.items.set(id, userDetails);
    self.selected = self.items.get(id);
  });

  const createUser = flow(function*(userDetails) {
    self.state = 'loading';

    yield UserAPI.create(userDetails);

    self.state = 'loaded';
  });

  const editUser = flow(function*(id, userDetails) {
    self.state = 'loading';

    yield UserAPI.edit(id, userDetails);

    self.state = 'loaded';
  });

  const clearSelected = () => {
    self.selected = null;
  };

  return {
    login,
    getUser,
    editUser,
    createUser,
    fetchUsers,
    afterCreate,
    clearSelected,
    readFromLocalStorage
  };
};
