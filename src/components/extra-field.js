import '../style/components/extra-field.css';

import CheckboxField from './checkbox-field';
import FormattedCurrency from './formatted-currency';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const ExtraField = ({
  className,
  displayName,
  name,
  onBlur,
  onChange,
  price
}) => (
  <div className={classNames('extra-field', { [className]: !!className })}>
    <div className="extra-field__action">
      <CheckboxField name={name} onChange={onChange} />
    </div>

    <div className="extra-field__details extra-details">
      <div className="extra-details__name">{displayName}</div>

      {price && (
        <div className="extra-details__price">
          <FormattedCurrency {...price} />
        </div>
      )}
    </div>
  </div>
);

ExtraField.propTypes = {
  className: PropTypes.string,
  displayName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  price: PropTypes.shape({
    currency: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })
};

ExtraField.defaultProps = {
  className: null,
  price: null
};

export default ExtraField;
