import { httpClient } from './http-client';

export const OrderAPI = {
  async create(orderDetails) {
    try {
      const { status } = await httpClient.post('/orders', orderDetails);
      return status === 201;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async list() {
    try {
      const { data } = await httpClient.get('/orders');
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
};
