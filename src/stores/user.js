import { User } from '../models/user';
import { UserActions } from '../actions/user';
import { types } from 'mobx-state-tree';

const initialState = {
  state: 'loaded',
  selected: null
};

export const UserStore = types
  .model({
    state: types.enumeration('State', ['loading', 'loaded', 'error']),
    selected: types.maybe(User)
  })
  .actions(UserActions)
  .views(self => ({
    get isLoading() {
      return self.state === 'loading';
    }
  }));

export const UserStoreInstance = UserStore.create(initialState);
