import '../style/components/product-form.css';

import React, { Component } from 'react';
import { isEmpty } from 'ramda';

import { Formik } from 'formik';
import TextField from '../components/text-field';
import Button from '../components/button';
import PropTypes from 'prop-types';

class ProductForm extends Component {
  constructor() {
    super();

    this.handleAddExtraField = this.handleAddExtraField.bind(this);
    this.handleExtraFieldChange = this.handleExtraFieldChange.bind(this);
    this.renderForm = this.renderForm.bind(this);

    this.state = {
      extras: []
    };
  }

  componentWillMount() {
    this.setState({
      extras: !isEmpty(this.props.initialValues.extras)
        ? this.props.initialValues.extras
        : []
    });
  }

  handleAddExtraField() {
    this.setState({
      extras: this.state.extras.concat({
        name: '',
        price: { currency: 'BRL', value: '' }
      })
    });
  }

  handleExtraFieldChange = (idx, fieldName) => evt => {
    const newExtras = this.state.extras.map((extra, sidx) => {
      if (idx !== sidx) return extra;
      return { ...extra, [fieldName]: evt.target.value };
    });

    this.setState({ extras: newExtras });
  };

  renderExtraFields() {
    const content = this.state.extras.map((extra, key) => (
      <div className="extras-container__details" key={key}>
        <TextField
          value={extra.name}
          name={`extras[${key}].name`}
          placeholder="Nome do item adicional"
          onChange={this.handleExtraFieldChange(key, 'name')}
        />
        <TextField
          value={extra.price.value}
          name={`extras[${key}].price.value`}
          placeholder="Preço"
          onChange={this.handleExtraFieldChange(key, 'price.value')}
        />
      </div>
    ));

    return (
      <div className="extras-container">
        {content}
        <Button
          type="button"
          message="+ adicionais"
          onClick={this.handleAddExtraField}
          className="extras-container__addButton"
        />
      </div>
    );
  }

  renderProductFields(errors) {
    const categoryError = errors.category
      ? errors.category.name
      : errors.category;

    return (
      <div>
        <TextField error={errors.name} name="name" placeholder="Nome do item" />
        <TextField name="description" placeholder="Descrição do item" />
        <TextField
          error={categoryError}
          name="category.name"
          placeholder="Nome da categoria"
        />
        <TextField
          error={errors.price ? errors.price.value : errors.price}
          name="price.value"
          placeholder="Preço"
        />
        <TextField
          error={errors.thumbnail}
          name="thumbnail"
          placeholder="URL da imagem do produto"
        />
      </div>
    );
  }

  renderForm({ errors, handleSubmit }) {
    const { isInEditMode } = this.props;
    const buttonMessage = isInEditMode ? 'Editar Produto' : 'Cadastrar Produto';

    return (
      <form className="product-form" onSubmit={handleSubmit}>
        <h2 className="product-form__title">Produto</h2>
        {this.renderProductFields(errors)}

        <h2 className="product-form__title">Adicionais</h2>
        {this.renderExtraFields()}

        <Button type="submit" message={buttonMessage} />
      </form>
    );
  }

  render() {
    const { initialValues, validationSchema, handleSubmit } = this.props;

    return (
      <div className="product-form">
        <Formik
          validationSchema={validationSchema}
          initialValues={initialValues}
          onSubmit={handleSubmit}
          render={this.renderForm}
        />
      </div>
    );
  }
}

ProductForm.propTypes = {
  initialValues: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  validationSchema: PropTypes.object,
  isInEditMode: PropTypes.bool
};

ProductForm.defaultProps = {
  isInEditMode: false
};

export default ProductForm;
