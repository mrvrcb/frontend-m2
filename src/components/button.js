import '../style/components/button.css';

import PropTypes from 'prop-types';
import React from 'react';

const Button = ({ className, message, type, onClick }) => (
  <button className={className} type={type} onClick={onClick}>
    <span>{message}</span>
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string.isRequired,
  type: PropTypes.string,
  onClick: PropTypes.func
};

Button.defaultProps = {
  className: null
};

export default Button;
