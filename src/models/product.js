import { Extra } from './extra';
import { Money } from './money';
import { types } from 'mobx-state-tree';

export const Product = types.model({
  id: types.identifier(types.string),
  thumbnail: types.string,
  name: types.string,
  description: types.string,
  price: types.maybe(Money),
  extras: types.optional(types.map(Extra), {}),
  category: types.maybe(
    types.model({
      id: types.identifier(types.string),
      name: types.string
    })
  )
});
