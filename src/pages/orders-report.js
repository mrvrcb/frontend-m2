import '../style/pages/orders-report.css';

import React, { Component } from 'react';

import ReactTable from 'react-table';
import 'react-table/react-table.css';

import FormattedCurrency from '../components/formatted-currency';

const reportsData = [
  {
    user: { email: 'xablau@mussum.com', firstName: 'Xablau', lastName: 'Bla' },
    total: { currency: 'BRL', value: 30.0 },
    product: {
      name: 'Bolo de Laranja',
      price: { currency: 'BRL', value: 22.0 }
    },
    extras: [
      { name: 'Cobertura', price: { currency: 'BRL', value: 4.0 } },
      { name: 'Grenolado', price: { currency: 'BRL', value: 4.0 } }
    ]
  },
  {
    user: { email: 'mussum@xablau.com', firstName: 'Mussum', lastName: 'Xa' },
    total: { currency: 'BRL', value: 24.0 },
    product: { name: 'Cookies', price: 20.0 },
    extras: [{ name: 'Nuttela', price: { currency: 'BRL', value: 4.0 } }]
  }
];

const reportsColumns = [
  {
    Header: 'Usuário',
    accessor: 'user.email'
  },
  {
    Header: 'Nome',
    accessor: 'user',
    Cell: user => {
      const { firstName, lastName } = user.value;
      return <span>{`${firstName} ${lastName}`}</span>;
    }
  },
  {
    Header: 'Produto',
    accessor: 'product.name'
  },
  {
    Header: 'Adicionais',
    accessor: 'extras',
    Cell: extras => {
      const extraNames = extras.value.map(extra => extra.name);
      return <span>{extraNames.join(', ')}</span>;
    }
  },
  {
    Header: 'Preço Total',
    accessor: 'total',
    Cell: total => <FormattedCurrency {...total.value} />
  }
];

class OrdersReport extends Component {
  renderFullname() {
    const { firstName, lastName } = this.props.user;
    const fullName = `${firstName} ${lastName}`;

    return <span>{fullName}</span>;
  }

  render() {
    return (
      <div className="orders-report">
        <h1>Relatório de Encomendas</h1>
        <ReactTable
          data={reportsData}
          columns={reportsColumns}
          defaultPageSize={10}
          filterable
          className="-striped"
          previousText="Anterior"
          nextText="Próximo"
          loadingText="Carregando..."
          noDataText="Nenhum dado encontrado"
          pageText="Página"
          ofText="de"
          rowsText="linhas"
        />
      </div>
    );
  }
}

export default OrdersReport;
