import { FormattedNumber } from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';

const FormattedCurrency = ({ currency, value }) => (
  <FormattedNumber
    value={value}
    style="currency" // eslint-disable-line
    currency={currency}
  />
);

FormattedCurrency.propTypes = {
  currency: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired
};

export default FormattedCurrency;
