import '../style/pages/create-product.css';

import * as yup from 'yup';

import React, { Component } from 'react';
import { inject, observer, propTypes } from 'mobx-react';

import ProductForm from '../components/product-form';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

class CreateProduct extends Component {
  state = {
    extras: [{ name: '', price: { currency: 'BRL', value: '' } }]
  };

  handleSubmit = values => {
    this.props.productStore.createProduct(values).then(() => {
      this.props.history.push('/home');
    });
  };

  render() {
    const initialValues = {
      name: '',
      description: '',
      price: { currency: 'BRL', value: '' },
      thumbnail: '',
      category: {
        name: ''
      },
      extras: [{ name: '', price: { currency: 'BRL', value: '' } }]
    };

    const validationSchema = yup.object().shape({
      name: yup.string().required('* Obrigatório'),
      description: yup.string(),
      price: yup.object().shape({
        currency: yup.string(),
        value: yup
          .number()
          .typeError('Preço deve ser um número')
          .positive('Preço deve ser um número positivo')
          .required('* Obrigatório')
      }),
      thumbnail: yup.string().required('* Obrigatório'),
      category: yup.object().shape({
        name: yup.string().required('* Obrigatorio')
      })
    });

    return (
      <div className="create-product">
        <h1>Criação de Produto</h1>
        <ProductForm
          validationSchema={validationSchema}
          initialValues={initialValues}
          handleSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

CreateProduct.propTypes = {
  productStore: PropTypes.shape({
    items: propTypes.observableMap.isRequired,
    state: PropTypes.string.isRequired,
    selected: PropTypes.object
  }),
  history: PropTypes.object
};

const enhance = compose(
  inject('productStore'),
  observer
);

export default withRouter(enhance(CreateProduct));
