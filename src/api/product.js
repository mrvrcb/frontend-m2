import { httpClient } from './http-client';

export const ProductAPI = {
  async delete(id) {
    try {
      const { status } = await httpClient.delete(`/products/${id}`);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async edit(id, values) {
    try {
      const { status } = await httpClient.put(`/products/${id}`, values);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async create(productDetails) {
    try {
      const { status } = await httpClient.post('/products', productDetails);
      return status === 204;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async get(id) {
    try {
      const { data } = await httpClient.get(`/products/${id}`);
      return data;
    } catch (error) {
      console.error(error);
      return {};
    }
  },

  async list() {
    try {
      const { data } = await httpClient.get('/products');
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
};
