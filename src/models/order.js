import { Extra } from './extra';
import { types } from 'mobx-state-tree';

export const Money = types.model({
  userId: types.string,
  productId: types.string,
  selectedExtras: types.array(Extra)
});
