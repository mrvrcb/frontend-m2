import { Category } from '../models/category';
import { CategoryActions } from '../actions/category';
import { isNil } from 'ramda';
import { types } from 'mobx-state-tree';

const initialState = {
  items: {},
  state: 'loaded',
  selected: null
};

export const CategoryStore = types
  .model({
    items: types.map(Category),
    state: types.enumeration('State', ['loading', 'loaded', 'error']),
    selected: types.maybe(types.reference(Category))
  })
  .actions(CategoryActions)
  .views(self => ({
    get hasSelectedCategory() {
      return !isNil(self.selected);
    }
  }));

export const CategoryStoreInstance = CategoryStore.create(initialState);
