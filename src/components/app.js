import '../style/components/app.css';

import { Redirect, Route, Switch } from 'react-router-dom';

import CreateProduct from '../pages/create-product.js';
import EditProduct from '../pages/edit-product.js';
import Home from '../pages/home.js';
import Login from '../pages/login.js';
import OrdersReport from '../pages/orders-report.js';
import ProductDetails from '../pages/product-details.js';
import React from 'react';
import Register from '../pages/register.js';
import Sidebar from './sidebar.js';

const App = () => (
  <div className="app">
    <Sidebar className="app__sidebar" />

    <div className="app__main">
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/create-product" component={CreateProduct} />
        <Route exact path="/orders-report" component={OrdersReport} />

        <Route exact path="/edit-product/:id" component={EditProduct} />
        <Route path="/products/:id" component={ProductDetails} />

        <Redirect from="/" to="/home" />
      </Switch>
    </div>
  </div>
);

export default App;
