import '../style/pages/home.css';

import React, { Component } from 'react';
import { inject, observer, propTypes } from 'mobx-react';

import ProductCard from '../components/product-card';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { values } from 'ramda';

class Home extends Component {
  static propTypes = {
    productStore: PropTypes.shape({
      items: propTypes.observableMap.isRequired,
      state: PropTypes.string.isRequired
    })
  };

  componentWillMount() {
    this.props.productStore.fetchProducts();
  }

  renderProducts = () => {
    const { productStore } = this.props;
    const { items } = productStore;

    const products = values(items.toJSON());

    return (
      <div className="products-container">
        {products.map((product, key) => (
          <ProductCard
            key={product.id}
            id={product.id}
            name={product.name}
            description={product.description}
            price={product.price}
            thumbnail={product.thumbnail}
          />
        ))}
      </div>
    );
  };

  render() {
    return <div className="home">{this.renderProducts()}</div>;
  }
}

const enhance = compose(
  inject('productStore'),
  observer
);

export default enhance(Home);
