import '../style/pages/register.css';

import * as yup from 'yup';

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import Button from '../components/button';
import { Formik } from 'formik';
import Logo from '../components/logo';
import PropTypes from 'prop-types';
import TextField from '../components/text-field';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

yup.addMethod(yup.string, 'equalTo', (ref, msg) => {
  return yup.mixed().test({
    name: 'equalTo',
    exclusive: false,
    message: msg,
    params: {
      reference: ref.path
    },
    test: value => value === this.resolve(ref)
  });
});

class Register extends Component {
  handleSubmit = values => {
    delete values.confirmPassword;

    this.props.userStore.createUser(values);

    setTimeout(() => {
      this.props.history.push('/login');
    }, 500);
  };

  renderForm = ({ errors, handleSubmit }) => (
    <form className="login__form" onSubmit={handleSubmit}>
      <TextField error={errors.firstName} name="firstName" placeholder="Nome" />
      <TextField
        error={errors.lastName}
        name="lastName"
        placeholder="Sobrenome"
      />
      <TextField
        error={errors.phoneNumber}
        name="phoneNumber"
        placeholder="Telefone"
      />
      <TextField
        type="email"
        error={errors.email}
        name="email"
        placeholder="E-mail"
      />
      <TextField
        type="password"
        error={errors.password}
        name="password"
        placeholder="Senha"
      />
      <TextField
        type="password"
        error={errors.confirmPassword}
        name="confirmPassword"
        placeholder="Confirmar Senha"
      />

      <Button type="submit" message="Cadastrar-se" />
    </form>
  );

  render() {
    const initialValues = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      password: '',
      confirmPassword: ''
    };

    const validationSchema = yup.object().shape({
      firstName: yup.string().required('* Obrigatório'),
      lastName: yup.string().required('* Obrigatório'),
      phoneNumber: yup.string().required('* Obrigatório'),
      email: yup
        .string()
        .email('E-mail deve ser valido')
        .required('* Obrigatório'),
      password: yup.string().required('* Obrigatório'),
      confirmPassword: yup
        .string()
        .oneOf([yup.ref('password'), null], 'Senhas não coincidem')
        .required('* Obrigatório')
    });

    return (
      <div className="register">
        <div className="register__logo">
          <Logo black />
        </div>

        <Formik
          validationSchema={validationSchema}
          initialValues={initialValues}
          onSubmit={this.handleSubmit}
          render={this.renderForm}
        />
      </div>
    );
  }
}

Register.propTypes = {
  userStore: PropTypes.shape({
    state: PropTypes.string.isRequired,
    selected: PropTypes.object
  })
};

const enhance = compose(
  inject('userStore'),
  observer
);

export default withRouter(enhance(Register));
