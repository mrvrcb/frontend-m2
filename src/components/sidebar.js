import '../style/components/sidebar.css';

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import Logo from './logo.js';
import PropTypes from 'prop-types';
import SidebarItem from './sidebar-item';
import { compose } from 'recompose';
import { contains } from 'ramda';

class Sidebar extends Component {
  state = {
    expandSelected: false
  };

  handleExpandSidebarClick = () => {
    this.setState({
      expandSelected: !this.state.expandSelected
    });
  };

  get isAdminProfile() {
    const { selected } = this.props.userStore;

    if (!selected) {
      return false;
    }

    return contains('ADMIN', selected.roles.map(role => role.name));
  }

  get isUserLogged() {
    const { selected } = this.props.userStore;

    if (!selected) {
      return false;
    }

    return true;
  }

  render() {
    const { className } = this.props;

    return (
      <nav id="sidebar" className={`sidebar ${className}`}>
        <div className="sidebar__mobile-content">
          <div className="sidebar__logo">
            <Logo />
          </div>
          <div
            onClick={this.handleExpandSidebarClick}
            className="sidebar__expand-sidebar"
          >
            {this.state.expandSelected ? <span>&times;</span> : <span>☰</span>}
          </div>
        </div>

        <ul
          className={`sidebar__items ${
            this.state.expandSelected ? 'show' : ''
          }`}
        >
          <li className="sidebar__item">
            <SidebarItem to="/home">Home</SidebarItem>
          </li>

          {this.isAdminProfile && (
            <li className="sidebar__item">
              <SidebarItem to="/create-product">Criar Produto</SidebarItem>
            </li>
          )}

          {this.isAdminProfile && (
            <li className="sidebar__item">
              <SidebarItem to="/orders-report">
                Relatório de Encomendas
              </SidebarItem>
            </li>
          )}

          {this.isUserLogged && (
            <li className="sidebar__item">
              <SidebarItem to="/login">Login</SidebarItem>
            </li>
          )}

          {this.isUserLogged && (
            <li className="sidebar__item">
              <SidebarItem to="/register">Cadastro</SidebarItem>
            </li>
          )}
        </ul>
      </nav>
    );
  }
}

Sidebar.propTypes = {
  className: PropTypes.string
};

const enhance = compose(
  inject('userStore'),
  observer
);

export default enhance(Sidebar);
