import '../style/pages/edit-product.css';

import * as yup from 'yup';

import React, { Component } from 'react';
import { inject, observer, propTypes } from 'mobx-react';

import ProductForm from '../components/product-form';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

class EditProduct extends Component {
  componentWillMount() {
    const { match } = this.props;
    const { params } = match;
    const { id } = params;

    this.props.productStore.getDetails(id);
  }

  handleSubmit = productDetails => {
    const { productStore } = this.props;
    const {
      selected: { id }
    } = productStore;

    productStore.editProduct(id, productDetails);

    setTimeout(() => {
      this.props.history.push(`/products/${id}`);
    }, 500);
  };

  renderForm = () => {
    const { productStore } = this.props;
    const { selected } = productStore;

    const initialValues = selected.toJSON();
    const validationSchema = yup.object().shape({
      name: yup.string().required('* Obrigatório'),
      description: yup.string(),
      price: yup.object().shape({
        currency: yup.string(),
        value: yup
          .number()
          .typeError('Preço deve ser um número')
          .positive('Preço deve ser um número positivo')
          .required('* Obrigatório')
      }),
      thumbnail: yup.string().required('* Obrigatório'),
      category: yup.object().shape({
        name: yup.string().required('* Obrigatorio')
      })
    });

    return (
      <ProductForm
        validationSchema={validationSchema}
        initialValues={initialValues}
        handleSubmit={this.handleSubmit}
        isInEditMode
      />
    );
  };

  render() {
    const { productStore } = this.props;
    const { isLoading } = productStore;

    return (
      <div className="edit-product">
        <h1>Edição de Produto</h1>
        {!isLoading && this.renderForm()}
      </div>
    );
  }
}

EditProduct.propTypes = {
  productStore: PropTypes.shape({
    items: propTypes.observableMap.isRequired,
    state: PropTypes.string.isRequired,
    selected: PropTypes.object
  })
};

const enhance = compose(
  inject('productStore'),
  observer
);

export default withRouter(enhance(EditProduct));
