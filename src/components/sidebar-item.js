import '../style/components/sidebar-item.css';

import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

const SidebarItem = ({ children, icon, to }) => (
  <NavLink
    to={to}
    className="sidebar-item"
    activeClassName="sidebar-item--active"
  >
    <div className="sidebar-item__content">{children}</div>
  </NavLink>
);

SidebarItem.propTypes = {
  children: PropTypes.node,
  icon: PropTypes.string,
  to: PropTypes.string.isRequired
};

export default SidebarItem;
