import '../style/pages/login.css';

import * as yup from 'yup';

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import Button from '../components/button';
import { Formik } from 'formik';
import Logo from '../components/logo';
import TextField from '../components/text-field';
import { compose } from 'recompose';

class Login extends Component {
  handleSubmit = values => {
    this.props.userStore.login(values.username, values.password).then(() => {
      this.props.history.push('/home');
    });
  };

  renderForm = ({ errors, handleSubmit }) => (
    <form className="login__form" onSubmit={handleSubmit}>
      <TextField
        error={errors.username}
        name="username"
        placeholder="Usuário"
      />

      <TextField
        type="password"
        error={errors.password}
        name="password"
        placeholder="Senha"
      />

      <Button type="submit" message="Login" />
    </form>
  );

  render() {
    const initialValues = {
      username: '',
      password: ''
    };

    const validationSchema = yup.object().shape({
      username: yup.string().required('* Obrigatório'),
      password: yup.string().required('Preencha o campo para continuar')
    });

    return (
      <div className="login">
        <div className="login__logo">
          <Logo black />
        </div>

        <Formik
          validationSchema={validationSchema}
          initialValues={initialValues}
          onSubmit={this.handleSubmit}
          render={this.renderForm}
        />
      </div>
    );
  }
}

const enhance = compose(
  inject('userStore'),
  observer
);

export default enhance(Login);
