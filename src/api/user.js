import { httpClient } from './http-client';

export const UserAPI = {
  async create(productDetails) {
    try {
      const { status } = await httpClient.post('/user', productDetails);
      return status === 204;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async get() {
    try {
      const { data } = await httpClient.get('/user', { withCredentials: true });
      return data;
    } catch (error) {
      console.error(error);
      return {};
    }
  }
};
