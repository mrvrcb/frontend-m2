import { types } from 'mobx-state-tree';

export const Money = types.model({
  currency: types.string,
  value: types.number
});
