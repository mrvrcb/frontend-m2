import '../style/components/text-field.css';

import { Field } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const TextField = ({
  className,
  type,
  error,
  name,
  placeholder,
  value,
  onChange
}) => (
  <div className="text-field">
    <Field
      className={classNames('text-field__field', {
        'text-field--error': !!error
      })}
      type={type}
      name={name}
      placeholder={placeholder}
    />

    {error && <div className="text-field__error">{error}</div>}
  </div>
);

TextField.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

TextField.defaultPropss = {
  type: 'text',
  className: null,
  error: null
};

export default TextField;
