import { indexBy, prop } from 'ramda';

import { ProductAPI } from '../api/product';
import { flow } from 'mobx-state-tree';

export const ProductActions = self => {
  const fetchProducts = flow(function*() {
    self.state = 'loading';

    const items = yield ProductAPI.list();
    self.state = 'loaded';
    self.items = indexBy(prop('id'), items);
  });

  const getDetails = flow(function*(id) {
    self.state = 'loading';

    const product = yield ProductAPI.get(id);
    const productDetails = {
      ...product,
      extras: indexBy(prop('id'), product.extras)
    };

    self.state = 'loaded';
    self.items.set(id, productDetails);
    self.selected = self.items.get(id);
  });

  const createProduct = flow(function*(productDetails) {
    self.state = 'loading';

    yield ProductAPI.create(productDetails);

    self.state = 'loaded';
  });

  const editProduct = flow(function*(id, productDetails) {
    self.state = 'loading';

    yield ProductAPI.edit(id, productDetails);

    self.state = 'loaded';
  });

  const clearSelected = () => {
    self.selected = null;
  };

  return {
    getDetails,
    fetchProducts,
    createProduct,
    clearSelected,
    editProduct
  };
};
