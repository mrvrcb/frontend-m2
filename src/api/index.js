import { CategoryAPI } from './category';
import { ExtrasAPI } from './extras';
import { LoginAPI } from './login';
import { OrderAPI } from './order';
import { ProductAPI } from './product';
import { UserAPI } from './user';

export { OrderAPI, ExtrasAPI, ProductAPI, CategoryAPI, UserAPI, LoginAPI };
