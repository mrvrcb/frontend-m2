import { httpClient } from './http-client';

export const ExtrasAPI = {
  async delete(id) {
    try {
      const { status } = await httpClient.delete(`/extras/${id}`);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  },

  async edit(id, values) {
    try {
      const { status } = await httpClient.put(`/extras/${id}`, values);
      return status === 200;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
};
