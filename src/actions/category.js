import { indexBy, prop } from 'ramda';

import { CategoryAPI } from '../api/category';
import { flow } from 'mobx-state-tree';

export const CategoryActions = self => {
  const fetchCategories = flow(function*() {
    self.state = 'loading';

    const items = yield CategoryAPI.list();
    self.state = 'loaded';
    self.items = indexBy(prop('id'), items);
  });

  const getCategory = flow(function*(id) {
    self.state = 'loading';

    const item = yield CategoryAPI.get(id);

    self.state = 'loaded';
    self.items.set(id, item);
    self.selected = self.items.get(id);
  });

  const clearSelected = () => {
    self.selected = null;
  };

  return {
    getCategory,
    clearSelected,
    fetchCategories
  };
};
