import { Product } from '../models/product';
import { ProductActions } from '../actions/product';
import { types } from 'mobx-state-tree';

const initialState = {
  items: {},
  state: 'loaded',
  selected: null
};

export const ProductStore = types
  .model({
    items: types.map(Product),
    state: types.enumeration('State', ['loading', 'loaded', 'error']),
    selected: types.maybe(types.reference(Product))
  })
  .actions(ProductActions)
  .views(self => ({
    get isLoading() {
      return self.state === 'loading';
    }
  }));

export const ProductStoreInstance = ProductStore.create(initialState);
