import '../style/components/product-card.css';

import React, { Component } from 'react';

import DefaultThumbnail from '../images/default-thumbnail.png';
import FormattedCurrency from '../components/formatted-currency';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

class ProductCard extends Component {
  handleProductClick = () => {
    const { history, id } = this.props;
    history.push(`/products/${id}`);
  };

  render() {
    const { thumbnail, name, description, price } = this.props;

    return (
      <div className="product-card" onClick={this.handleProductClick}>
        <img className="product-card__image" src={thumbnail} alt="Produto" />

        <div className="product-card__information product-details">
          <div className="product-details__title">{name}</div>
          <div className="product-details__description">{description}</div>
          <div className="product-details__price">
            <FormattedCurrency {...price} />
          </div>
        </div>
      </div>
    );
  }
}

ProductCard.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.shape({
    currency: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  }),
  thumbnail: PropTypes.string,
  handleClick: PropTypes.func,
  history: PropTypes.object
};

ProductCard.defaultProps = {
  thumbnail: DefaultThumbnail
};

export default withRouter(ProductCard);
