import { types } from 'mobx-state-tree';

export const User = types.model({
  email: types.identifier(types.string),
  firstName: types.string,
  lastName: types.string,
  phoneNumber: types.string,
  roles: types.array(
    types.model({
      name: types.string,
      authorities: types.array(types.string)
    })
  )
});
