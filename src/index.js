import './style/main.css';

import App from './components/app.js';
import { BrowserRouter } from 'react-router-dom';
import { CategoryStoreInstance } from './stores/category';
import { IntlProvider } from 'react-intl';
import { ProductStoreInstance } from './stores/product';
import { Provider } from 'mobx-react';
import React from 'react';
import { UserStoreInstance } from './stores/user';
import registerServiceWorker from './registerServiceWorker';
import { render } from 'react-dom';

const storeProviders = {
  productStore: ProductStoreInstance,
  categoryStore: CategoryStoreInstance,
  userStore: UserStoreInstance
};

const Root = () => (
  <IntlProvider locale="en">
    <BrowserRouter>
      <Provider {...storeProviders}>
        <App />
      </Provider>
    </BrowserRouter>
  </IntlProvider>
);

render(<Root />, document.getElementById('root'));
registerServiceWorker();
